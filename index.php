<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Project\Constants,
    Bitrix\Main\Loader,
    Bitrix\Main\Data\Cache;

$currentCityId = "10";
$coursesIblockId = Constants::IB_COURSES;
$shedulesIblockId = Constants::IB_SHEDULES;

$cache = Cache::createInstance();
$cacheTime = 36000;

$arResult = [];

if ($cache->initCache($cacheTime, $currentCityId)) { 
    $arResult = $cache->getVars();
}
elseif ($cache->startDataCache()) {
    if (!Loader::includeModule("iblock")) {
        throw new SystemException("Модуль инфоблоков не установлен");
        $cache->abortDataCache();
    }

    $arCourcesId = [];

    $arCoursesFilter = ["IBLOCK_ID" => $coursesIblockId, "PROPERTY_CITY" => $currentCityId];
    $rsCourses = CIBlockElement::GetList([], $arCoursesFilter, false, false, ["ID"]);

    while ($arCourse = $rsCourses->Fetch()) {
        $arCourcesId[] = $arCourse["ID"];
    }

    if (!empty($arCourcesId)) {
        $arShedulesFilter = [
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $shedulesIblockId,
            ">=ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL"),
            "PROPERTY_COURCE_ID" => $arCourcesId
        ];
        $arShedulesSelect = ["ID", "ACTIVE_FROM", "ACTIVE_TO", "PROPERTY_COURCE_ID"];
        $rsShedules = CIBlockElement::GetList([], $arShedulesFilter, false, false, $arShedulesSelect);

        while ($arShedule = $rsShedules->Fetch()) {
            $arResult[] = $arShedule;
        }
    }
    else {
        $cache->abortDataCache();
    }

    $cache->endDataCache(["arResult" => $arResult]);
}

echo "<pre>";
print_r($arResult);
echo "</pre>";
