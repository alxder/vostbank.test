<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader,
    Bitrix\Main\SystemException;

$agentInstall = \CAgent::AddAgent(
    "\Project\Agents::sendIblockElementEditLog();",
    "form",
    "N",
    1800,
    "",
    "Y",
    ConvertTimeStamp(time() + 1800, "FULL"),
    100
);

if ($agentInstall)
    echo "<p>Агент успешно установлен</p>";
else
    echo "<p>Ошибка установки агента</p>";

if (!Loader::includeModule("form")) {
    throw new SystemException("Модуль веб-форм не установлен");
}

$arFormParams = [
    "NAME"   => "Логи редактирования элементов инфоблоков",
    "SID"    => "IBLOCK_ELEMENT_EDIT_LOG",
    "C_SORT" => 100,
    "BUTTON" => "Сохранить",
    "arSITE" => ["s1"],
    "arMENU" => ["ru" => "Логи редактирования элементов инфоблоков"]
];

$formId = \CForm::Set($arFormParams);

if ($formId > 0) {
    $arTemplates = \CForm::SetMailTemplate($formId);

    if ($arTemplates) {
        \CForm::Set(["arMAIL_TEMPLATE" => $arTemplates], $formId);
    }

    $arFormFieldsParams = [
        "SID"                 => "LOG_FILE",
        "ACTIVE"              => "Y",
        "C_SORT"              => 100,
        "TITLE"               => "Файл лога",
        "FORM_ID"             => $formId,
        "REQUIRED"            => "Y",
        "TITLE_TYPE"          => "text",
        "ADDITIONAL"          => "N",
        "FILTER_TITLE"        => "Файл лога",
        "IN_EXCEL_TABLE"      => "N",
        "IN_RESULTS_TABLE"    => "Y",
        "RESULTS_TABLE_TITLE" => "Файл лога",
        "arANSWER"            => [
            0 => [
                "MESSAGE"     => "Файл лога",
                "C_SORT"      => 100,
                "ACTIVE"      => "Y",
                "FIELD_TYPE"  => "file",
                "FIELD_PARAM" => "file"
            ]
        ]
    ];

    $fieldId = \CFormField::Set($arFormFieldsParams);

    if ($fieldId > 0) {
        echo "<p>Добавлена веб-форма с ID ".$formId."</p>";
    }
}
else {
    echo $strError;
}