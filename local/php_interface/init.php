<?
use Bitrix\Main\Loader,
    Bitrix\Main\EventManager;

Loader::registerAutoLoadClasses(null, [
    "Project\Constants" => "/local/php_interface/classes/constants.php",
    "Project\Handlers" => "/local/php_interface/classes/handlers.php",
    "Project\Agents" => "/local/php_interface/classes/agents.php"
]);

EventManager::getInstance()->addEventHandler("iblock", "OnAfterIBlockElementUpdate", ["Project\\Handlers", "afterIBlockElementUpdate"]);
