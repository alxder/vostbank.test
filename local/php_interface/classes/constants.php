<?
namespace Project;

use Bitrix\Main\Loader;

class Constants
{
    const LOG_PATH = "/local/logs/iblockElementEditLog.txt";
    const LOG_WEBFORM_ID = 6;
    const LOG_WEBFORM_FIELD = "form_file_6";

    const IB_NEWS = 1;
    const IB_COURSES = 2;
    const IB_SHEDULES = 3;
}