<?
namespace Project;

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\SystemException;

Loc::loadMessages(__FILE__);

class Agents
{
    public static function sendIblockElementEditLog() {
        if (!Loader::includeModule("form")) {
            throw new SystemException(Loc::getMessage("MODULE_WEBFORM_NOT_INSTALLED"));
            return false;
        }

        global $strError;

        $webFormId = Constants::LOG_WEBFORM_ID;
        $logFullPath = $_SERVER["DOCUMENT_ROOT"].Constants::LOG_PATH;
        $webFormField = Constants::LOG_WEBFORM_FIELD;

        if (file_exists($logFullPath)) {
            $arLogFile = \CFile::MakeFileArray($logFullPath);

            $arFormParams = [
                $webFormField => $arLogFile
            ];

            if ($resultId = \CFormResult::Add($webFormId, $arFormParams)) {
                if (\CFormResult::Mail($resultId)) {
                    unlink($logFullPath);
                }
                else {
                    echo $strError;
                }
            }
            else {
                echo $strError;
            }
        }

        return "\Project\Agents::sendIblockElementEditLog();";
    }
}