<?
namespace Project;

use Bitrix\Main\Loader,
    Bitrix\Main\Diag\Debug;

class Handlers
{
    public static function afterIBlockElementUpdate(&$arFields) {
        if (!Loader::includeModule("form")) {
            throw new \Bitrix\Main\SystemException("Модуль веб-форм не установлен");
        }

        global $USER;

        $logText = "#".$arFields["ID"]." ".$USER->GetFullName()." ".date("d.m.Y H:i:s");
        $logPath = Constants::LOG_PATH;

        Debug::writeToFile($logText, false, $logPath);
    }
}